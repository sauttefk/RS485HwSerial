- V1.1.0
  * Adopt Teensy's API for setting RS485 transmit enable pin instead of overloading begin
- V1.0.0
  * Initial release
